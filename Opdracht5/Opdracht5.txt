Studentnummer:
Naam:

Verschilt de Terminal in Visual Studio Code met Git Bash?
	# Nee

Waar worden Branches vaak voor gebruikt?
	# Feature / Bugfix

Hoe vaak ben je in Opdracht 5A van Branch gewisseld?
	# 2x

Vul de volgende commando's aan:
 -Checken op welke branch je aan het werken bent:
	# git branch
 -Nieuwe Branch aanmaken
	# git branch (nieuwe branch naam)
 -Van Branch wisselen
	# git checkout (branchnaam)
 -Branch verwijderen
        # git branch -d (branchnaam)